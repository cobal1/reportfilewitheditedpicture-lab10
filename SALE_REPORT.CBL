       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SALE-REPORT.
       AUTHOR. PEERAYA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "shop_receipts.dat"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
           SELECT 200-OUTPUT-FILE ASSIGN TO "sale_report.rpt"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-OUTPUT-FILE-STATUS.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  INPUT-HEAD-BUFFER.
           05 RECORD-TYPE-CODE     PIC X.
              88 IS-HEAD-RECORD    VALUE "H".
              88 IS-DETAIL-RECORD  VALUE "S".
           05 SHOP-ID              PIC X(5).
           05 FILLER               PIC X(30).
       01  INPUT-DETAIL-BUFFER.
           05 RECORD-TYPE-CODE     PIC X.
           05 ITEM-ID              PIC X(8).
           05 QTY-SOLD             PIC 9(3).
           05 ITEM-COST            PIC 999V99.
           05 FILLER               PIC X(19).
       FD  200-OUTPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  OUTPUT-BUFFER           PIC X(80).

       WORKING-STORAGE SECTION. 
       01  WS-INPUT-FILE-STATUS    PIC X(2).
           88 FILE-OK              VALUE "00".
           88 FILE-AT-END          VALUE "10".
       01  WS-OUTPUT-FILE-STATUS   PIC X(2).
           88 FILE-OK              VALUE "00".
           88 FILE-AT-END          VALUE "10".
       01  WS-CALCULATION.
           05 WS-QTY-TOTAL         PIC 9(5)    VALUE ZEROS.
           05 WS-COST-TOTAL        PIC 9(5)V99 VALUE ZEROS.
           05 WS-INPUT-COUNT       PIC 9(5)    VALUE ZEROS.
       01  RPT-FORMAT.
           05 RPT-SHOP-HEADER.
              10 FILLER            PIC X(13)   VALUE "---Shop #ID  ".
              10 RPT-SHOP-ID       PIC X(5).
              10 FILLER            PIC X(20)   
                 VALUE "--------------------".
           05 RPT-SHOP-TITLE.
              10 FILLER            PIC X(37)
                 VALUE "Name                Qty          COST".
           05 RPT-SHOP-DETAIL.
              10 RPT-ITEM-ID       PIC X(8).
              10 FILLER            PIC X(12)   VALUE SPACES.
              10 RPT-QTY-SOLD      PIC ZZ9.
              10 FILLER            PIC X(7)    VALUE SPACES.
              10 RPT-ITEM-COST     PIC $$$9.99.
           05 RPT-SHOP-END.
              10 FILLER            PIC X(18)
                 VALUE "Total sales       ".
              10 RPT-QTY-TOTAL     PIC ZZZZ9   VALUE ZEROS.
              10 FILLER            PIC X(4)    VALUE SPACES.
              10 RPT-COST-TOTAL    PIC $$$,$$9.99.

       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT 
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
           PERFORM 3000-END THRU 3000-EXIT
           GOBACK.

       1000-INITIAL.
           PERFORM 1100-OPEN-FILE-INPUT THRU 1100-EXIT 
           PERFORM 1200-OPEN-FILE-OUTPUT THRU 1200-EXIT 
           PERFORM 8000-READ THRU 8000-EXIT
           .
       1000-EXIT.
           EXIT.

       1100-OPEN-FILE-INPUT.
           OPEN INPUT 100-INPUT-FILE 
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY '******* SALE REPORT ABEND *******'
      *       ABEND = ABNORMAL END
                 UPON CONSOLE 
      *       UPON CONSOLE >> SHOW REPORT ON ADMIN SCREEN
              DISPLAY '* PARA 1100-OPEN-FILE-INPUT FAIL *'
                 UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS ' *'
                 UPON CONSOLE 
              DISPLAY '******* SALE REPORT ABEND *******'
                 UPON CONSOLE 
              STOP RUN 
           END-IF 
           .
       1100-EXIT.
           EXIT.

       1200-OPEN-FILE-OUTPUT.
           OPEN OUTPUT 200-OUTPUT-FILE  
           IF FILE-OK OF WS-OUTPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY '******* SALE REPORT ABEND *******'
                 UPON CONSOLE 
              DISPLAY '* PARA 1200-OPEN-FILE-OUTPUT FAIL *'
                 UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-OUTPUT-FILE-STATUS ' *'
                 UPON CONSOLE 
              DISPLAY '******* SALE REPORT ABEND *******'
                 UPON CONSOLE 
              STOP RUN 
           END-IF 
           .
       1200-EXIT.
           EXIT.

       2000-PROCESS.
           PERFORM 2100-PROCESS-HEAD THRU 2100-EXIT
           PERFORM 2200-PROCESS-DETAIL THRU 2200-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 OR IS-HEAD-RECORD 
           PERFORM 2300-PROCESS-END THRU 2300-EXIT
           .
       2000-EXIT.
           EXIT
           .

       2100-PROCESS-HEAD.
           MOVE ZEROS TO WS-QTY-TOTAL 
           MOVE ZEROS TO WS-COST-TOTAL 
           MOVE SHOP-ID TO RPT-SHOP-ID 

           DISPLAY RPT-SHOP-HEADER 
           MOVE RPT-SHOP-HEADER TO OUTPUT-BUFFER 
           PERFORM 7000-WRITE THRU 7000-EXIT 

           DISPLAY RPT-SHOP-TITLE 
           MOVE RPT-SHOP-TITLE TO OUTPUT-BUFFER 
           PERFORM 7000-WRITE THRU 7000-EXIT
           PERFORM 8000-READ THRU 8000-EXIT
           .
       2100-EXIT.
           EXIT
           .

       2200-PROCESS-DETAIL.
           MOVE ITEM-ID TO RPT-ITEM-ID 
           MOVE ITEM-COST TO RPT-ITEM-COST 
           MOVE QTY-SOLD TO RPT-QTY-SOLD 
           DISPLAY RPT-SHOP-DETAIL 

           MOVE RPT-SHOP-DETAIL TO OUTPUT-BUFFER 
           PERFORM 7000-WRITE THRU 7000-EXIT 

           COMPUTE WS-QTY-TOTAL = WS-QTY-TOTAL + QTY-SOLD 
           COMPUTE WS-COST-TOTAL = 
                          WS-COST-TOTAL + (QTY-SOLD * ITEM-COST) 

           PERFORM 8000-READ THRU 8000-EXIT
           .
       2200-EXIT.
           EXIT
           .

       2300-PROCESS-END.
           MOVE WS-QTY-TOTAL TO RPT-QTY-TOTAL 
           MOVE WS-COST-TOTAL TO RPT-COST-TOTAL
           DISPLAY RPT-SHOP-END 
           MOVE RPT-SHOP-END TO OUTPUT-BUFFER 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           .
       2300-EXIT.
           EXIT
           .

       3000-END.
           DISPLAY "READ COUNT " WS-INPUT-COUNT 
           CLOSE 100-INPUT-FILE 200-OUTPUT-FILE
           .
       3000-EXIT.
           EXIT
           .

       7000-WRITE.
           WRITE OUTPUT-BUFFER 
           IF FILE-OK OF WS-OUTPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY '******* SALE REPORT ABEND *******'
                 UPON CONSOLE 
              DISPLAY '* PARA 7000-WRITE FAIL *'
                 UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-OUTPUT-FILE-STATUS ' *'
                 UPON CONSOLE 
              DISPLAY '******* SALE REPORT ABEND *******'
                 UPON CONSOLE 
              STOP RUN 
           END-IF 
           .
       7000-EXIT.
           EXIT
           .

       8000-READ.
           READ 100-INPUT-FILE
           IF FILE-OK OF WS-INPUT-FILE-STATUS
              ADD 1 TO WS-INPUT-COUNT
           ELSE IF FILE-AT-END OF WS-INPUT-FILE-STATUS
              CONTINUE
           ELSE
              DISPLAY '******* SALE REPORT ABEND *******'
      *       ABEND = ABNORMAL END
                 UPON CONSOLE 
      *       UPON CONSOLE >> SHOW REPORT ON ADMIN SCREEN
              DISPLAY '* PARA 8000-READ FAIL *'
                 UPON CONSOLE 
              DISPLAY '* FILE STATUS = ' WS-INPUT-FILE-STATUS ' *'
                 UPON CONSOLE 
              DISPLAY '******* SALE REPORT ABEND *******'
                 UPON CONSOLE 
              STOP RUN 
           END-IF 
           .
       8000-EXIT.
           EXIT
           .
